# Node.js Tech Assignment

## Problem description

A mind map is a diagram used to visually organize information. It is often created around a central topic to which associated representations of ideas such as images, words and parts of words are added. Subtopics are connected directly to the central topic, and related ideas branch out from those subtopics.

An example of a mind map can be seen below:

![](mindmap.png)

Imagine you are building an application that creates mind maps. In this application, all concepts (central topic, subtopic, related ideas, etc) are represented by elements and associated to a mind map project.

## The assignemnt

Create a REST API with a single GET endpoint which returns a paginated list of projects with an optional filter for user ID.

Create a production-ready implementation using Node.js, Docker and your choice of libraries and frameworks. Use the mocked APIs listed below to retrieve project and element information to form your response.

An example of query using the query parameter `user_id=5fb5d7d5e77b3749e751906c` should return the following:

```
[
    {
        _id: "5fb55700730639bcaaf9024e",
        title: "Words",
        description: "A list of meaningful words",
        elements: [
            {
                _id: "5fb55d72fcce2c6a00e90e39",
                title: "Idea",
                description: "A thought or suggestion as to a possible course of action",
            },
            {
                _id: "5fb55d7e3963cb95c53a2c58",
                title: "Concept",
                description: "An abstract idea; a general notion",
            },
            {
                _id: "5fb55d85bec06754045af6d9",
                title: "Thought",
                description: "An idea or opinion produced by thinking, or occurring suddenly in the mind",
            },
            {
                _id: "5fb55d89a78d3bc939bff0ac",
                title: "Aspiration",
                description: "A strong desire, longing, or aim",
            },
            {
                _id: "5fb55d8ccc433b1de8f33080",
                title: "Goal",
                description: "The object of a person's ambition or effort; an aim or desired result",
            },
        ]
    }
]
```

Hint: Don't forget to dockerize your application, write documentation, tests, etc.

### Projects API

Returns a list of projects. Optional filters include `user_id`.

GET `https://nodejs-tech-assignment.herokuapp.com/projects?user_id=5fb5d7d5e77b3749e751906c`

```
[
  {
    "_id": "5fb55700730639bcaaf9024e",
    "title": "Words",
    "description": "A list of meaningful words",
    "user_id": "5fb5d7d5e77b3749e751906c",
    "rootElement": "5fb55d85bec06754045af6d9"
  }
]
```

P.S.: The API may take a couple of seconds to respond the first time you query it

### Elements API

Returns a list of elements. Optional filters include `project_id`.

GET `https://nodejs-tech-assignment.herokuapp.com/elements?project_id=5fb55700730639bcaaf9024e`

```
[
  {
    "_id": "5fb55d72fcce2c6a00e90e39",
    "project_id": "5fb55700730639bcaaf9024e",
    "title": "Idea",
    "description": "A thought or suggestion as to a possible course of action",
    "parentElement": null,
    "createdDate": "2020-10-15T01:53:52.180+00:00",
    "lastModifiedDate": "2020-10-15T01:53:52.180+00:00"
  },
  {
    "_id": "5fb55d7e3963cb95c53a2c58",
    "project_id": "5fb55700730639bcaaf9024e",
    "title": "Concept",
    "description": "An abstract idea; a general notion",
    "parentElement": "5fb55d72fcce2c6a00e90e39",
    "createdDate": "2020-10-15T01:53:52.180+00:00",
    "lastModifiedDate": "2020-10-15T01:53:52.180+00:00"
  },
  {
    "_id": "5fb55d85bec06754045af6d9",
    "project_id": "5fb55700730639bcaaf9024e",
    "title": "Thought",
    "description": "An idea or opinion produced by thinking, or occurring suddenly in the mind",
    "parentElement": "5fb55d7e3963cb95c53a2c58",
    "createdDate": "2020-10-15T01:53:52.180+00:00",
    "lastModifiedDate": "2020-10-15T01:53:52.180+00:00"
  },
  {
    "_id": "5fb55d89a78d3bc939bff0ac",
    "project_id": "5fb55700730639bcaaf9024e",
    "title": "Aspiration",
    "description": "A strong desire, longing, or aim",
    "parentElement": "5fb55d85bec06754045af6d9",
    "createdDate": "2020-10-15T01:53:52.180+00:00",
    "lastModifiedDate": "2020-10-15T01:53:52.180+00:00"
  },
  {
    "_id": "5fb55d8ccc433b1de8f33080",
    "project_id": "5fb55700730639bcaaf9024e",
    "title": "Goal",
    "description": "The object of a person's ambition or effort; an aim or desired result",
    "parentElement": "5fb55d89a78d3bc939bff0ac",
    "createdDate": "2020-10-15T01:53:52.180+00:00",
    "lastModifiedDate": "2020-10-15T01:53:52.180+00:00"
  }
]
```

P.S.: The API may take a couple of seconds to respond the first time you query it

## Submission

Create a public repository in github, gitlab, or bitbucket and push your code alongside a README.md file with clear instructions on how to set up the environment and run your code. Once you are done, send the link back to the interviewer who assigned you the challenge via email. An acknowledgment will be sent to you after we receive the solution.

Please, don't use "GENAIZ" in the repository name or description.
